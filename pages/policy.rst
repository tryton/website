.. title: Policy

This section describes the policy for “Tryton Community”,
which is much more relaxed the “Tryton Core”'s policy.
“Community” members and contributors will decide on a more
detailed governance as the project evolves.

To make the project attractive for many, only a few rule apply:

* Each project must include a `README` file describing the project's
  purpose.  Typically two or three sentences suffice — while one
  sentence is to brief most times.

  For modules the file shall explain all the requirements and
  procedures to include such modules (this will allow evolution in the
  future).

* Projects must be licensed under a free or open source license.
  AGPLv3-or-later, GPLv3-or-later, LGPLv3-or-later, FDLv1.3-or-later
  are recommended.

* Software projects must provide a minimal test suite within 3 month
  after creation of the project and is required to pass.  For modules
  the test suite must at least include the standard
  ``ModuleTestSuite``.

* Following the `Tryton coding guidelines
  <https://www.tryton.org/develop/guidelines/code>`_
  is recommended.

* For modules:

  - New projects are required to support the latest major version of
    Tryton.

  - Per each released series (version X.y) an extra branch is created.

  - No database changes on released series (to grant stability).

* Each project in ”Community” can establish its own processes
  and special rules
  as long as these are not contrary to the rules of “Tryton Community”,
  the Tryton project or the Heptapod FOSS hosting service.

And these non-rules apply:

* There is no strict requirement on modules quality.  If other
  “Community” members are not satisfied with the quality, they are
  invited to improve the design step by step.

* New modules can be added to any supported series,
  but we require to have a copy also working for the latest major
  version.

* No requirement for the maintainers to migrate modules to newer series:
  Each module will be migrated on best effort basis.
  Modules not migrated just won't be available for new series
  until someone steps forward and migrates them.

  Modules not migrated to any of the currently supported Tryton major
  versions will be marked and ”unmaintained” and may be moved to
  the attic.

* Projects are not required to us Mercurial as version control system
  (even if using Mercurial is recommended, esp. for modules).

* Projects are free to manage several related modules
  in one repo (like a small mono-repo).
