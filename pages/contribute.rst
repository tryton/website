.. title: Become Part of the Tryton Community

We are about to build up the infrastructure to make
“Tryton Community“ work.

We are seeking:

* a web-designer improving this web site

* translators for the website and the
  `Tryton Book <https://www.tryton-dach.org/das-tryton-buch/>`_

* persons improving the CI/CD at heptapod

* Tryton modules

* tools for Tryton, e.g. Installers

Join our efforts by moving your repos to
https://foss.heptapod.net/tryton-community
