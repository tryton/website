.. title: Governance and Moderation/Maintainer Team

“Tryton Community” will be moderated and
the repositories technically owned by a team of moderators.
The team will be elected “somehow” [to be discussed] and
the period of office is expected to be no longer than two years.

Initial maintainers are htgoebel, pokoli and yangoon.


Moderator's responsibilities, rights and obligations
-----------------------------------------------------

Maintainers are more like moderators.
They are expected to foster the aims of “Community”
but are *not* expected to maintain any of the repositories.
(Of course, they can maintain, if they want.)

Maintainers should have a good idea of what’s going on,
but the other responsibilities can (and should! :-)) be delegated.

There are some duties:

* Enforcing “Tryton Community” to be a warm and welcoming place:
  maintainers are the contact point for anyone who wants to report abuse.

* Enforcing “Community” policies (see above).

* Making decisions, about code or anything, when consensus cannot be
  reached.

* Technically owning and managing the “Community” group
  of the Tryton Heptapod group.

* Decide about accepting new projects for “Community” and
  creating a respective sub-project in Heptapod.

* Maintainers are allowed to add or remove access to/from sub-projects.
  This is especially true for projects that seem to be unmaintained.
