.. title: Tryton Community

“Tryton Community” (in short “Community”)
is open for all projects related to Tryton:
modules, tools, documentation, etc.,
making them more visible and easier to access.

“Tryton Community” aims to make it attractive to collaborate on Tryton
modules and tools, esp. to avoid duplicate work.  “Community” shall be a
place where a vital and agile Tryton Community can grow.
“Community” shall be warm and welcoming.  “Community” will provide central
development services to help contributors developing high-quality
projects.
