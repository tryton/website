.. title: Operations

.. note::
   Whether the ideas scratched here will be implemented actually
   depends on how active contributors are.
   Also the services provides by “Community” may evolve over time.

To make it easy for contributed projects,
“Community” will provide central development services like:
Issue tracking and Merge requests, some Translation service,
CI and automated testing,
repository or project templates (e.g. using cookiecutter), etc.

* CI / testing → Labels on projects


* In the repository we include the CI definition (following upstream
  design)

* In the CI respository we include the script to release modules.

* packaged (PyPI, release, whatever)

  * not part of the Docker image

  * provide an easy way to build a Dockerimage containing custom
    selected Community modules


